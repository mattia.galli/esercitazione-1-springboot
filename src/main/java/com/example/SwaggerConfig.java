

package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


// http://localhost:8080/swagger-ui/

@Configuration //indica al framework in fase di bootstrap di eseguire il metodo
public class SwaggerConfig {

    @Bean //esegue questo metodo
    public Docket api() { // method chain (cascata)
        return new Docket(DocumentationType.OAS_30)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
                //.forCodeGeneration(true);
    }
}

