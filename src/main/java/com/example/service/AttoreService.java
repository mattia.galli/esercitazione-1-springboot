package com.example.service;

import java.util.List;
import java.util.Optional;

import com.example.domain.Attore;
import com.example.repository.AttoreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service //Logica di business
public class AttoreService {
    @Autowired
    AttoreRepository attoreRepository; //id del repository

    public List<Attore> findAll(){ //il metodo che restituisce una lista con tutti gli attori
        return attoreRepository.findAll();
    }

    //restituisce un oggetto di tipo optional<Attore>:1 isPresent()=true 2) false non trovato
    public Optional<Attore> findById(Long codAttore){
        return attoreRepository.findById(codAttore);
    }

    public void save(Attore attore){
        attoreRepository.save(attore);
    }

    public Optional<Attore> deleteById(Long codAttore){
        Optional<Attore> optAttore =  attoreRepository.findById(codAttore);
        if (optAttore.isPresent()){
            attoreRepository.deleteById(codAttore);
            return optAttore;
        }
        return Optional.empty();
    }

    public Optional<Attore> put(Attore attore){
        Optional<Attore> optAttore= attoreRepository.findById(attore.getCodAttore());
    
        if(optAttore.isPresent()){
            attoreRepository.save(attore);
            return optAttore;
        }
            return Optional.empty();
    }

    public List<Attore> findAllPage(PageRequest request){
        return attoreRepository.findAll(request).getContent();
    }

}
