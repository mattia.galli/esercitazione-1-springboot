package com.example.repository;

import com.example.domain.Attore;
import com.example.repository.AttoreRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttoreRepository extends JpaRepository<Attore, Long>{
    
}
