package com.example.component;

import java.util.Objects;

import org.springframework.stereotype.Component;

@Component
public class MiaClasse {
    String titolo = "Sono una classe";


    public MiaClasse() {
    }

    public MiaClasse(String titolo) {
        this.titolo = titolo;
    }

    public String getTitolo() {
        return this.titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public MiaClasse titolo(String titolo) {
        setTitolo(titolo);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof MiaClasse)) {
            return false;
        }
        MiaClasse miaClasse = (MiaClasse) o;
        return Objects.equals(titolo, miaClasse.titolo);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(titolo);
    }

    @Override
    public String toString() {
        return "{" +
            " titolo='" + getTitolo() + "'" +
            "}";
    }

}
