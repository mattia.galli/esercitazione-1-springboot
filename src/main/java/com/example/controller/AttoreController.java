package com.example.controller;

import com.example.component.MiaClasse;
import com.example.domain.Attore;
import com.example.dto.AttoriPar;
import com.example.service.AttoreService;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api/attori")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AttoreController {
    
    // @Autowired //scorciatoia per DI
    AttoreService attoreService;
    MiaClasse miaClasse;
    //Equivalente dell'autowired
    public AttoreController (AttoreService attoreService,
                             MiaClasse miaClasse){
        this.attoreService = attoreService;
        this.miaClasse = miaClasse;
    }

    @GetMapping("/component/c")
    public ResponseEntity<?> c(){
        return new ResponseEntity<String>(miaClasse.getTitolo(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<List<Attore>>(attoreService.findAll(),HttpStatus.OK);
    }

    @GetMapping("/{codAttore}")
    public ResponseEntity<?> findById(@PathVariable("codAttore") Long codAttore){

        Optional<Attore> optAttore = attoreService.findById(codAttore);
        if (optAttore.isPresent()){
            Attore attore = optAttore.get();
            return new ResponseEntity<Attore>(attore,HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    public ResponseEntity<?> save(@RequestBody Attore attore){
        attoreService.save(attore);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @DeleteMapping("/{codAttore}")
    public ResponseEntity<?> deleteById(@PathVariable("codAttore") Long codAttore) {
        Optional<Attore> optAttore=attoreService.deleteById(codAttore);
        if (optAttore.isPresent()){
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

   @PutMapping()
    public ResponseEntity<?> put(@RequestBody Attore attore){
        Optional<Attore>optAttore=attoreService.put(attore);
        if(optAttore.isPresent()){
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/")
    public ResponseEntity<List<Attore>> attori(
    @RequestParam(required=false, name="page", defaultValue="0") int page,
    @RequestParam(required=false, name="size", defaultValue="10") int size,
    @RequestParam(required=false, name="ascdesc", defaultValue="asc") String ascdesc,
    @RequestParam(required=false, name="field", defaultValue="codAttore") String field) {
            
        Sort sort = Sort.by(field);
        if  (ascdesc.equals("desc")) 
            sort = sort.descending();
        PageRequest request = PageRequest.of(page, size, sort);
        return new ResponseEntity<List<Attore>>(attoreService.findAllPage(request), HttpStatus.OK);
    }


    @PostMapping(path="/dto/{id")
    public void dto(@RequestBody AttoriPar par, @PathVariable Long id){
        System.out.println(par);
    }
    
}
