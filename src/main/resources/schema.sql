drop table if exists attori;
create table attori(
    cod_attore      bigint auto_increment primary key,
    anno_nascita    bigint,
    country         varchar(255),
    nome            varchar(255)
);